## netikras/docker:dind-mitmcache

FROM docker:dind

ENV MITMCACHE_CA_URL=""
ENV CONTAINERS_INHERIT_PROXY="true"

RUN : "Prepare config.json template for proxy settings" \
    && apk add envsubst \
    && mkdir -p /root/.docker \
    && cat >/root/.docker/config.json.proxy.template <<EOF
{
 "proxies": {
   "default": {
     "httpProxy": "\${http_proxy}",
     "httpsProxy": "\${https_proxy}",
     "noProxy": "\${no_proxy}"
   }
 }
}
EOF

CMD ["sh", "-exec", \
    "\
        [ -n \"${MITMCACHE_CA_URL}\" ] && { wget --no-check-certificate \"${MITMCACHE_CA_URL}\" -O- | tee /etc/ssl/certs/mitmcache-ca-cert.pem; update-ca-certificates; }; \
        [ \"${CONTAINERS_INHERIT_PROXY}\" = true ] && { cat /root/.docker/config.json.proxy.template | envsubst | tee /root/.docker/config.json; } ; \
        dockerd-entrypoint.sh" \
    ]
