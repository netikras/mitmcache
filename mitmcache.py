import json
import os
import re
import tempfile
import time

# import yaml
# noinspection PyUnresolvedReferences
from mitmproxy import http, ctx

"""
TODO
1. configurable: gzip content
2. yaml configuration support
"""

DEFAULT_CONFIG = {
    "cache": {
        "type": "fs",  # fs | mem
        "dir": "/tmp/playground/mitm_cache",
        "default_rule": {
            "cached": True,
            "ttl": 3600,
            "evict_unfit": True,
            "respect_pragma": True,
            "compress": False,
        },
        "self": {
            "domain": "mitmcache",
            "freeform": {
                "enabled": True,
                "path": "/key"
            },
            "ca": {
                "enabled": True,
                "path": "/ca"
            },
            "health": {
                "enabled": True,
                "path": "/health"
            }
        }
    },
    "rules": {
        "GET": {
            "^https?://127.0.0.1(:\\d+)?/health/?$": {
                "static": {
                    "ref": "health-OK",
                    "body": "OK"
                }
            }
        },
        ".*": {
            "^https?://mitmcache/?.*": {
                "cached": True,
            },
            "^https?://auth.docker.io/.*": {
                "cached": False,
                "evict_unfit": True,
            },
        },
        "^(GET|HEAD)$": {
            "^https?://jsnexus.johnstonesupply.com(:8443)?/repository/.+$": {
                "respect_pragma": False,
            },
        },
    },
    "static": {
        "health-OK": {
            "code": 200,
            "headers": {
                "Content-Type": "text/plain"
            },
            "body": "",
        }
    }
}

HEADERS_CACHE_HIT = {"X-Cache": "HIT"}
HEADERS_CACHE_MISS = {"X-Cache": "MISS"}


class Cached:
    def __init__(self, data, rule=None, ts=time.time()):
        if rule is None:
            rule = DEFAULT_CONFIG.get("cache", {}).get("default_rule", {})
        self.data = data
        self.rule = rule
        self.ts = ts


class Cache:
    def has_key(self, key) -> bool:
        return self.get(key) is not None

    def get(self, key, dflt=None) -> Cached:
        pass

    def put(self, key, cached: Cached):
        pass

    def evict(self, *keys):
        pass


class TtlCache(Cache):
    def __init__(self, ttl_sec):
        self.ttl = ttl_sec

    def has_key(self, key) -> bool:
        pass

    def _get_raw(self, key) -> Cached | None:
        pass

    def get(self, key, dflt=None) -> Cached | None:
        cached = self._get_raw(key)
        if cached is not None:
            ttl = cached.rule.get("ttl", self.ttl)
            if ttl == -1 or cached.ts + ttl > time.time():
                return cached
            else:
                if cached.rule.get("evict_unfit", True):
                    self.evict(key)
        if dflt is not None:
            return Cached(dflt, ts=time.time())
        else:
            return None


class InMemoryTtlCache(TtlCache):
    def __init__(self, ttl_sec):
        super().__init__(ttl_sec)
        self._cache = {}

    def has_key(self, url) -> bool:
        return url in self._cache

    def _get_raw(self, key) -> Cached | None:
        return self._cache.get(key, None)

    def put(self, key, cached: Cached) -> None:
        self._cache[key] = cached

    def evict(self, *keys) -> None:
        for key in keys:
            if key in self._cache:
                del self._cache[key]


class FilesystemTtlCache(TtlCache):
    def __init__(self, cfg: dict):
        default_rule = cfg.get("default_rule", {})
        super().__init__(default_rule.get("ttl", 3600))
        self.default_rule = default_rule
        self.cache_dir = cfg.get("dir", tempfile.gettempdir() + "/mitmcache")
        if not os.path.exists(self.cache_dir):
            os.makedirs(self.cache_dir)

    def has_key(self, key) -> bool:
        if key is None:
            return False
        path = self._get_cache_path(key)
        return os.path.exists(path) and not self.is_expired(key, path=path)

    def _get_cache_path(self, key) -> str | None:
        if key is None:
            return None
        decoded_path = key
        normalized_path = os.path.normpath(decoded_path)
        normalized_path = normalized_path.replace("/../", "/")
        normalized_path = normalized_path.replace("/./", "/")
        safe_path = os.path.join(self.cache_dir, normalized_path)
        ctx.log.info(f"CACHE_PATH: {safe_path}")
        return safe_path

    def is_expired(self, key, path=None, ttl=None) -> bool:
        if path is None:
            path = self._get_cache_path(key)
        if ttl is None:
            ttl = self.ttl
            if os.path.exists(path + ".rule"):
                with open(path + ".rule", "r") as rule_file:
                    rule = json.loads(rule_file.read())
                    ttl = int(rule.get("ttl", self.ttl))

        mtime = os.path.getmtime(path)
        expired = mtime + ttl < time.time()
        if expired:
            ctx.log.info(f"EXPIRED: {path}")
            return True
        else:
            return False

    def _get_raw(self, key) -> Cached | None:
        if key is None:
            return None
        rule = self.default_rule
        path = self._get_cache_path(key)

        if not os.path.exists(path):
            return None
        if self.is_expired(key, path=path):
            return None

        with open(path, 'rb') as data_file:
            data = data_file.read()
            # if isinstance(data, bytes):
            #     data = data.decode("utf-8")

        if os.path.exists(path + ".rule"):
            with open(path + ".rule", "rb") as rule_file:
                rule = json.loads(rule_file.read())

        cached = Cached(data, rule=rule, ts=os.path.getmtime(path))
        return cached

    def put(self, key, cached: Cached):
        if key is None:
            return
        if cached is None or cached.data is None:
            return
        path = self._get_cache_path(key)
        os.makedirs(os.path.dirname(path), exist_ok=True)

        with open(path, 'wb') as data_file:
            data = cached.data
            if isinstance(data, str):
                data = data.encode()
            data_file.write(data)

        if cached.rule is not None and cached.rule is not self.default_rule:
            with open(path + ".rule", 'w') as rule_file:
                rule_file.write(json.dumps(cached.rule))

    def evict(self, *keys):
        for key in keys:
            ctx.log.info(f"EVICT: {key}")
            if key is None:
                return
            path = self._get_cache_path(key)
            for path in [path, path + ".rule"]:
                if os.path.exists(path):
                    os.remove(path)


class TTLAwareCacheAddon:
    def __init__(self):
        self.config = None
        self.cache = None

        self._reload(DEFAULT_CONFIG)

    def _reload(self, cfg: dict = None) -> None:
        if cfg is None:
            cfg = self.config

        global DEFAULT_CONFIG
        DEFAULT_CONFIG = cfg
        self.config = cfg

        cache_type = cfg.get("cache", {}).get("type", "fs")
        if cache_type == "fs":
            self.cache = FilesystemTtlCache(cfg["cache"])
        elif cache_type == "mem":
            ttl = cfg.get("cache", {}).get("ttl", 3600)
            self.cache = InMemoryTtlCache(ttl)
        else:
            raise ValueError(f"Unknown cache type: '{cache_type}'")

    @staticmethod
    def load(loader) -> None:
        loader.add_option(
            name="config",
            typespec=str,
            default="",
            help="Path to a custom config file",
        )

    def configure(self, updates) -> None:
        if "config" in updates:
            config_file_path = ctx.options.config
            if config_file_path and config_file_path != "":
                ctx.log.info(f"Loading rules from {config_file_path}")
                cfg = self._load_rules_from_file(config_file_path)
                self._reload(cfg)
                ctx.log.info(f"Rules have been loaded")

    @staticmethod
    def _load_rules_from_file(path: str) -> dict | None:
        if path.endswith('.json'):
            with open(path, 'rt') as file:
                data = json.load(file)
                return data
        # elif path.endswith('.yaml') or path.endswith('.yml'):
        #     with open(path, 'rt') as file:
        #         data = yaml.safe_load(file)
        #         return data
        else:
            raise ValueError("Unsupported file format. Please use JSON or YAML.")

    def request(self, flow: http.HTTPFlow) -> None:
        ctx.log.info(f"::: REQUEST: {flow.request.method} {flow.request.pretty_url}")

        key = self.to_key(flow.request.method, flow.request.pretty_url)

        if self.addressed_to_self(flow.request):
            flow.response = self.handle_self_request(flow.request)
            if flow.response is not None:
                flow.request.headers["Pragma"] = "no-cache"
                return
            else:
                flow.response = http.Response.make(400, "mitmcache cannot process the request", {"Pragma": "no-cache"})

        rule = self.get_rule(flow.request)
        if "static" in rule:
            static_resp = rule.get("static", {})
            flow.response = http.Response.make(static_resp.get("code", 404),
                                               static_resp.get("body", "NOP"),
                                               static_resp.get("headers", {}))
            flow.request.headers["Pragma"] = "no-cache"
        if self._cache_control_disables_cache(flow.request, flow.response, rule):
            ctx.log.info(f"CACHE_CONTROL_skip_cache: {flow.request.pretty_url}")
            return

        if self.cache.has_key(f"{key}.code"):
            if bool(rule.get("cached", True)):
                headers = json.loads(self.cache.get(f"{key}.headers", {}).data) if self.cache.has_key(
                    f"{key}.headers") else {}
                headers["X-Cache"] = "HIT"
                code = int(self.cache.get(f"{key}.code").data)
                body = self.cache.get(f"{key}.body").data if self.cache.has_key(f"{key}.body") else ""
                flow.response = http.Response.make(code, body, headers)
                ctx.log.info(f"HIT: [{len(body)}] {flow.request.pretty_url}")
                return
            else:
                ctx.log.info(f"NO_CACHED: {flow.request.pretty_url}")
        else:
            ctx.log.info(f"MISS: {flow.request.pretty_url}")

    def response(self, flow: http.HTTPFlow) -> None:
        ctx.log.info(f"::: RESPONSE: {flow.request.method} {flow.request.pretty_url}")
        rule = self.get_rule(flow.request)
        if self._cache_control_disables_cache(flow.request, flow.response, rule):
            ctx.log.info(f"CACHE_CONTROL_skip_cache: {flow.request.pretty_url}")
            flow.response.headers["X-Cache"] = "MISS"
            return
        if flow.response.status_code < 400:
            key = self.to_key(flow.request.method, flow.request.pretty_url)
            if not self.cache.has_key(f"{key}.code"):
                if rule.get("cached", True):
                    self.cache.put(f"{key}.code", Cached(str(flow.response.status_code), rule))
                    self.cache.put(f"{key}.headers", Cached(json.dumps(dict(flow.response.headers.items())), rule))
                    self.cache.put(f"{key}.body", Cached(flow.response.content, rule))
                    ctx.log.info(f"CACHING: [{len(flow.response.content or '')}] {flow.request.pretty_url}")
                    flow.response.headers["X-Cache"] = "MISS"
                    return
                else:
                    ctx.log.info(f"RULE_DONT_CACHE: {flow.request.pretty_url}")
            else:
                # HIT
                pass
        else:
            ctx.log.info(f"RULE_BAD_CODE_DONT_CACHE: {flow.response.status_code} {flow.request.pretty_url}")

    def _cache_control_disables_cache(self, request: http.Request = None, response: http.Response = None,
                                      rule: dict = None) -> bool:
        if rule is None:
            rule = self.config.get("cache", {}).get("default_rule", {})
        if not bool(rule.get("respect_pragma", True)):
            return False

        cache_control = response is not None and (
                response.headers.get("Cache-Control") or response.headers.get("Pragma")) \
                        or \
                        request is not None and (request.headers.get("Cache-Control") or request.headers.get("Pragma"))
        if cache_control is not None and "no-cache" in str(cache_control):
            return True
        return False

    """
    returns an effective rule, i.e. default + overrides (if any)
    """

    def get_rule(self, request: http.Request) -> dict:
        method: str = request.method
        url: str = request.pretty_url

        default_rule = self.config.get("cache", {}).get("default_rule", {})

        for method_rule, url_rules in self.config.get("rules", {}).items():
            if re.match(method_rule, method):
                for url_rule_pattern, url_rule in url_rules.items():
                    if re.match(url_rule_pattern, url):
                        rule = dict(default_rule | (url_rule or {}))
                        static_ref = rule.get("static", {}).get("ref", None)
                        static_404 = {"code": 404, "body": f"Static rule missing: {static_ref}"}
                        if static_ref:
                            static_resp = dict(self.config.get("static", {}).get(static_ref, {}))
                            static_resp = (static_resp | rule.get("static", {}))
                            if static_resp == {}:
                                static_resp = static_404
                            rule["static"] = static_resp
                        ctx.log.info(f"RULE_OVERRIDE: {method} {url} {json.dumps(rule, default=vars)}")
                        return rule
        return default_rule

    @staticmethod
    def to_key(method: str, url: str) -> str:
        parts = url.split("?", 1)
        url = parts[0]
        if url.endswith("/"):
            url = url[:-1]
        key = method + "/" + url
        return key

    def get_ca_cert(self) -> str | None:
        # Access mitmproxy's CA certificate
        cert_path = os.path.expanduser(f"{ctx.master.options.confdir}/mitmproxy-ca-cert.pem")
        if os.path.exists(cert_path):
            with open(cert_path, "rt") as ca_file:
                ca_cert = ca_file.read()
            return ca_cert
        else:
            return None

    def handle_freeform_cache_request(self, request: http.Request) -> http.Response | None:
        ffc_config = self.config.get("cache", {}).get("self", {}).get("freeform", {})

        if bool(ffc_config.get("enabled", True)):
            ffc_domain = ffc_config.get('domain', 'mitmcache')
            ffc_base_path = ffc_config.get('path', '/key')
            pattern = re.compile(rf"^https?://{ffc_domain}{ffc_base_path}/?([^?]*)\??(.*)")
            url_match = pattern.match(request.pretty_url)

            if url_match is not None:
                ctx.log.info(f"SELF_FFC_URL_MATCH: {request.pretty_url}")
                resource = url_match.group(1)
                params = url_match.group(2)

                params_rule = {}
                if params is not None:
                    for param in params.split("&"):
                        if "=" in param:
                            p_name, p_value = param.split("=", 1)
                            params_rule[p_name] = p_value
                        else:
                            params_rule[param] = None

                key = self.to_key("CACHE", request.pretty_url)
                method = str(request.method).upper()
                if method == "GET":
                    if self.cache.has_key(f"{key}.body"):
                        body = self.cache.get(f"{key}.body")
                        headers = json.loads(self.cache.get(f"{key}.headers", {}).data) \
                            if self.cache.has_key(f"{key}.headers") else {}
                        headers["X-Cache"] = "HIT"
                        ctx.log.info(f"HIT: [{len(body.data or '')}] {request.pretty_url}")
                        return http.Response.make(200, body.data or "", headers)
                    else:
                        return http.Response.make(404, f"FreeFormCache has no resource under {resource}",
                                                  HEADERS_CACHE_MISS)
                elif method == "DELETE":
                    self.cache.evict(f"{key}.body")
                    return http.Response.make(200, "", HEADERS_CACHE_HIT)
                elif method == "POST":
                    rule = self.get_rule(request) | params_rule
                    self.cache.put(f"{key}.body", Cached(request.content, rule=rule))
                    self.cache.put(f"{key}.headers", Cached(json.dumps(dict(request.headers.items())), rule=rule))
                    ctx.log.info(f"CACHED: [{len(request.content or '')}] {request.pretty_url}")
                    return http.Response.make(201, "", HEADERS_CACHE_HIT)
                elif method == "HEAD":
                    if self.cache.has_key(f"{key}.body"):
                        return http.Response.make(200, "", HEADERS_CACHE_HIT)
                    else:
                        return http.Response.make(204, "", HEADERS_CACHE_MISS)
                else:
                    return http.Response.make(400, f"FreeFormCache cannot handle method {method}. Use GET/POST/DELETE",
                                              HEADERS_CACHE_MISS)

            else:
                pass
            pass
        return None

    def addressed_to_self(self, request: http.Request) -> bool:
        self_domain = self.config.get("cache", {}).get("self", {}).get("domain", "mitmcache")
        return bool(re.match(f"^https?://{self_domain}/.*", request.pretty_url))

    def handle_self_request(self, request) -> http.Response | None:
        response = self.handle_freeform_cache_request(request) or \
                   self.handle_ca_request(request) or \
                   self.handle_health_request(request)
        if response is not None:
            return response

        return None

    def handle_ca_request(self, request: http.Request) -> http.Response | None:
        self_config = self.config.get("cache", {}).get("self", {})
        ca_config = self_config.get("ca", {})

        if bool(ca_config.get("enabled", True)):
            self_domain = self_config.get("domain", "mitmcache")
            ca_path = ca_config.get('path', '/ca')

            if request.method == "GET" and re.match(rf"^https?://{self_domain}{ca_path}/?$", request.pretty_url):
                ctx.log.info(f"SELF_CA_URL_MATCH: {request.pretty_url}")
                return http.Response.make(200, self.get_ca_cert() or "", {"Content-Type": "application/x-x509-ca-cert"})
        return None

    def handle_health_request(self, request: http.Request) -> http.Response | None:
        self_config = self.config.get("cache", {}).get("self", {})
        ca_config = self_config.get("health", {})

        if bool(ca_config.get("enabled", True)):
            self_domain = self_config.get("domain", "mitmcache")
            health_path = ca_config.get('path', '/health')
            if request.method == "GET" and re.match(rf"^https?://{self_domain}{health_path}/?$", request.pretty_url):
                ctx.log.info(f"SELF_HEALTH_URL_MATCH: {request.pretty_url}")
                return http.Response.make(200, "OK", {"Content-Type": "text/plain"})
        return None


addons = [TTLAwareCacheAddon()]
