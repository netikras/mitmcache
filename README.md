# MITMPROXY addon for caching through proxy

## About
mitmcache is a mitmproxy addon acting as a transparent HTTP/S cache. It is configurable and flexible and can be leveraged to speed up repetitive requests. Originally this addon was written to speed up CI jobs in one of the BitBucket-based projects I'm working on, because we all know how annoying BB caching and limitations are :) .

## Configuration
mitmcache can be configured by supplying a configuration json. Configuration is split into several main parts:
- cache - high level configuration for the caching engine. 
  - type - type of the cache to be used. Currently available values are: `fs` and `mem`.
  - dir - used by the `fs` (filesystem-based) cache. It's a directory on the cache engine's filesystem where cached data will be stored.
  - default_rule - this is the default rule applied for all the requests. The default rule's values can be overridden by individual rules (see below) for more fine-grained tuning.
  - self - this part configures mitmcache's self-handling of requests. Read more on this below.
- rules - method and url regex combination leading to a caching rule definition. These rules extend/override values in the default cache rule. The resulting combined rule is applied to every request matching the respective method+url patterns in the configuration. The rule is cached along with the response data and is respected upon cached response retrieval.
- static - a static responses' library. Responses can be referred to by theis keys in rules.

### Rules
A caching rule defines what's supposed to be done with the request's response. The default rule is applied to all the responses by default. Should the request match either of the method+url patterns listed in the configuration's `rules` section, the effective rule will be the combination of default_rule + overrides.
It's worth mentioning that only the first rule matching both method and url will be applied. The rules' list will not be traversed any further.

The rule's keys are the following:
- cached - (boolean) - whether cache should be used for the request. If `true`, the cache will be looked up for a response to the request. If a valid (non-expired) response is found, it will be returned immediately. Otherwise, request will be allowed to propagate, retrieved response will be cached for later use and returned to the requester
- ttl - (int) - number of seconds the cached response is valid for (-1 == store indefinitely). Cached responses older than this age will be treated as cache misses: the request will propagate, retrieved response will be cached (TTL will be reset) and returned.
- evict_unfit - (boolean) - should the cached response be unfit for use (e.g. expired TTL), should it be evicted from cache before propagating the request. This is useful considering responses with status code >=400 are not cached and won't overwrite expired entries.
- respect_pragma - (boolean) - whether or not the request's/response's headers `Pragma` and `Cache-Control` be taken into account when making the decision whether to propagate the request and/or cache the response
- static - (object) - a map of objects containing code, headers and body, i.e. static response data (e.g. "health-OK"). Should this object contain a "ref" attribute, the value of "ref" will be looked up in the `$.static` configuration. Any other values besides 'ref' will be used to override values inherited from the `$.static` library.
- compress - (boolean) - **NOT IMPLEMENTED** - whether to compress data stored locally (to preserve storage). If data in the response is already compressed (`Content-Encoding` header), nothing will be done to the payload; otherwise the payload will be compressed _at rest_. If the request allows for compressed data (`Accept-Encoding` contains `gzip`), compressed cached payload will be returned as-is, otherwise it will be extracted before sending out as a response.

#### Static responses

`$.static` part of the configuration contains a key-object mapping of static responses. These responses, if referred by any rule, will be used as-is in HTTP response. `static` attribute in a rule definition example:
```
{
  "ref": "resp-default", ## optional
  "code": 200,
  "body": "OK"
}
```

This means that a rule, matching the request, will automatically return HTTP:200 with body "OK" and whatever headers were specified in `$.static.resp-default`. If `$.static.resp-default` had another body configured, it will be ignored, because the rule overrides it with "OK". The same applies to other attributes of `static`


## Self-handling requests
The mitmcache itself can process several kinds of requests. This means, these requests will not be propagated and the mitmcache will itself return a response to them.
By default, mitmcache will capture and handle requests sent to a domain `mitmcache`. However, this domain is configurable


### FreeFormCache (FFC)
mitmcache can act as a temporary storage for various resources. All the same caching rules apply to FFC resources.

FFC mechanism captures requests sent to a particular (preconfigured) base URL and, depending on the method, either puts the payload into the cache or retrieves it from there.
Allowed methods:
- GET - retrieve the resource (200 -- cache-hit; 404 -- cache-miss)
- POST - upsert the resource
- DELETE - evict the resource
- HEAD - verify the existence of the resource (200 -- exists; 204 -- does not exist)

#### Configuration
- enabled - (boolean) - whether FFC is enabled
- domain - (string) - requests sent to this domain, combined with the `base_path`, will be captured by FFC and not propagated any further (assuming FFC is enabled)
- path - (string) - see above. Everything in the URL following after the `base_path` is considered a cache key for the data. Additional query parameters can be supplied too - tey will be used as a customized caching rule for the FFC entry.
#### Example
Assuming the default (example) configuration:
```shell
curl -siLk -x 127.0.0.1:8080 -XPOST http://mitmcache/key/very-secret-document/feweafdgrwww325ßdf4.doc -d "I did it, it was me!"
curl -siLk -x 127.0.0.1:8080 -XHEAD http://mitmcache/key/very-secret-document/feweafdgrwww325ßdf4.doc
curl -siLk -x 127.0.0.1:8080 -XGET http://mitmcache/key/very-secret-document/feweafdgrwww325ßdf4.doc
curl -siLk -x 127.0.0.1:8080 -XDELETE http://mitmcache/key/very-secret-document/feweafdgrwww325ßdf4.doc
```

### CA exposure
Since mitmcache works on both http and https and encrypts https with a self-signed CA certificate, various tools aren't very keen on trusting a self-signed certificate and by default won't work with mitmcache. To bypass this restriction, mitmcache exposes its CA certificate for downloading and storing locally. If done so, applications can be passed the CA cert for trust and/or the CA cert can be wired into the system's CA list.

#### Configuration
- enabled - (boolean) - whether or not the CA exposure is enabled
- path - (string) - under what path will the CA be exposed

## Build

### OCI image

There's a Dockerfile meant to build the mitmcache addon and bundle it with a mitmproxy binary. It utilizes 2 environment
variables allowing for configuration:

- MITMPROXY_OPTS -- additional `mitmproxy` CLI args
- MITMCACHE_CONFIG -- path to the `mitmcache` configuration file (.json)

By default mitmcache will start with a sample configuration, using standard mitmproxy configuration.

## Configuration

### Addon (server)

```shell
## Example rules
$ cat /tmp/mitmcache.json
{
  "cache": {
    "type": "fs",
    "dir": "/tmp/mitm_cache2",
    "default_rule": {
      "cached": true,
      "ttl": 3600,
      "evict_unfit": true,
      "respect_pragma": true,
      "compress": false
    },
    "self": {
      "domain": "mitmcache",
      "freeform": {
        "enabled": true,
        "path": "/key"
      },
      "ca": {
        "enabled": true,
        "path": "/ca"
      }
    }
  },
  "rules": {
    ".*": {
      "^https?://mitmcache/key/?.*": {
        "cached": true
      },
      "^https?://auth.docker.io/.*": {
        "cached": false,
        "evict_unfit": true
      }
    },
    "POST|PUT|PATCH|DELETE": {
      ".*": {
        "cached": false,
        "evict_unfit": false
      }
    }
  }
}

## Starting the MITMCache
$ mitmproxy -s devops/tools/mitm/mitmcache.py --set config=/tmp/mitmcache.json
```

### Clients
### Universal (more or less)
#### Environment variables
Many tools respect the proxy environment variables:
- http_proxy
- HTTP_PROXY
- https_proxy
- HTTPS_PROXY
- all_proxy
- ALL_PROXY
- no_proxy
- NO_PROXY

Yes, it is tedious to handle both the casings. Most Linux tools rely on lowercase versions since Adam and Eve. However, some newer / modern tools seem to be following new guidelines claiming that env variables should be in all-uppercase. Some of them implement a backward-compatible lowercase check, while some don't. So just to be safe -- export them all. Also, make sure to specify the correct protocols in values.

As for the no_proxy value, some tools understand CIDRs, some don't; some tools understand wildcard domains some don't. In this example I'm specifying the riskiest values at the end, hoping tools that fail to process them will take in effect the values they've already processed before the error occurred.

```shell
PROXY_HOST=127.0.0.1
PROXY_PORT=8080
PROXY="${PROXY_HOST}:${PROXY_PORT}"
http_proxy="http://${PROXY}"
HTTP_PROXY=${http_proxy}
https_proxy="https://${PROXY}"
HTTPS_PROXY=${https_proxy}
all_proxy=${http_proxy}
ALL_PROXY=${all_proxy}
no_proxy="docker,127.0.0.1,localhost,127.0.0.0/8"
NO_PROXY=${no_proxy}
export PROXY_HOST PROXY_PORT PROXY http_proxy HTTP_PROXY https_proxy HTTPS_PROXY all_proxy ALL_PROXY no_proxy NO_PROXY
#export http_proxy= HTTP_PROXY= https_proxy= HTTPS_PROXY= all_proxy= ALL_PROXY= no_proxy= NO_PROXY=
```

#### Proxy CA Certificate
Regardless of the configuration approach, you'll most likely require a proxy's CA certificate to be trusted by the system. 
```shell
## For alternative package names consult https://stackoverflow.com/a/77672453/23326534
#apk add ca-certificates 
apt install ca-certificates 
mkdir -p /usr/local/share
wget --no-check-certificate mitmcache/ca -O- | tee /usr/local/share/ca-certificates/mitmcache-ca-cert.crt
update-ca-certificates
```

The above commands will add the mitmcache's CA certificate into /etc/ssl/certs/ca-certificates.crt, which is widely used by many applications to validate the trust chain against.

### Docker

There are two places to configure Proxy in Docker:
- dockerd -- launching `dockerd` with HTTP_PROXY env vars will make docker pull images through the configured proxy
- ~/.docker/config.json -- [client configuration](https://docs.docker.com/network/proxy/#configure-the-docker-client); all new containers launched after (re)configuring `$.proxies.*` will inherit the HTTP_PROXY and similar env variables. I.e. these env vars will be pre-set when the container starts up

```shell
echo '[Service]
Environment="HTTP_PROXY=http://127.0.0.1:8080"
Environment="HTTPS_PROXY=https://127.0.0.1:8080"
Environment="NO_PROXY=localhost,127.0.0.1"
' | sudo tee /etc/systemd/system/docker.service.d/http-proxy.conf

sudo cp ~/.mitmproxy/mitmproxy-ca-cert.pem /usr/local/share/ca-certificates/mitmproxy-ca-cert.crt
sudo update-ca-certificates 

sudo systemctl restart docker
```

### curl
There are two ways to pass proxy settings to curl:
- via the `-x <proxy>` CLI argument
- via the http_proxy / https_proxy / no_proxy environment variables

```shell
curl -siLk -x 127.0.0.1:8080 google.com

## or
http_proxy=127.0.0.1:8080 https_proxy=${http_proxy} \
  curl -siLk google.com
```

### npm
npm respects the proxy env variables. However, it does not respect the CAs trusted by the system - instead, it uses its own truststore. To override this, assuming you have set up your system to trust the proxy CA, configure npm to use system's _cafile_:

```shell
npm config set cafile /etc/ssl/certs/ca-certificates.crt
npm install
```

### maven
As explained [here](https://stackoverflow.com/questions/21252800/how-to-tell-maven-to-disregard-ssl-errors-and-trusting-all-certs), maven not only has to be assigned java's http?.proxy(Port|Host) properties, but also the HTTP client has to be reverted to `wagon`. An alternative is to use [keytool](https://maven.apache.org/guides/mini/guide-repository-ssl.html) to generate a keystore and pass it to Maven, but that's a tad more involving of a procedure.

```shell
PROXY_HOST=127.0.0.1:8080
export MAVEN_OPTS="-Dhttp.proxyHost=${PROXY_HOST%%:*} -Dhttp.proxyPort=8080 -Dhttps.proxyHost=${PROXY_HOST%%:*} -Dhttps.proxyPort=8080 \
              -Dmaven.resolver.transport=wagon -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true"
```

