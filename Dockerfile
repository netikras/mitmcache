## netikras/mitmcache:latest

FROM mitmproxy/mitmproxy

WORKDIR /app

ENV MITMCACHE_CONFIG="/app/mitmcache.config.json"
ENV MITMPROXY_OPTS=""

ENV APP_USER=1007
ENV APP_GROUP=1007
ENV HOME=/app

RUN apt update

RUN : "Preinstall utilities" \
    && apt -y install curl procps less net-tools dnsutils \
    && rm -rf /var/lib/apt/lists/*

COPY mitmcache.py /app/
COPY mitmcache.config.example.json /app/mitmcache.config.json

RUN chown -R "${APP_USER}:${APP_GROUP}" /app/

HEALTHCHECK --interval=10s --timeout=4s CMD curl -sLkf http://127.0.0.1:8080/health

#USER ${APP_USER}
EXPOSE 8080

CMD ["/bin/sh", "-c", "mitmproxy ${MITMPROXY_OPTS} -s '/app/mitmcache.py' --set config=${MITMCACHE_CONFIG}"]
